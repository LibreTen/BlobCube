extends Node2D

enum d {UP,DOWN,LEFT,RIGHT}

var pos :int

func move(dir,count,camera_point):
	var tween = get_node(name+"/Tween")
	match dir:
		d.RIGHT:
			pos += count
			tween.interpolate_property(self, "position",
			position, position+Vector2(320*count, 0), 1,
			Tween.TRANS_CIRC, Tween.EASE_IN_OUT)
			if camera_point!=null:
				tween.interpolate_property(camera_point, "position",
				camera_point.position, camera_point.position+Vector2(320*count, 0), 1,
				Tween.TRANS_CIRC, Tween.EASE_IN_OUT)
		d.LEFT:
			pos -= count
			tween.interpolate_property(self, "position",
			position, position +Vector2(-320*count, 0), 1,
			Tween.TRANS_CIRC, Tween.EASE_IN_OUT)
			if camera_point!=null:
				tween.interpolate_property(camera_point, "position",
				camera_point.position, camera_point.position+Vector2(-320*count, 0), 1,
				Tween.TRANS_CIRC, Tween.EASE_IN_OUT)
		d.DOWN:
			pos += 5*count
			tween.interpolate_property(self, "position",
			position, position+Vector2(0,320*count), 1,
			Tween.TRANS_CIRC, Tween.EASE_IN_OUT)
			if camera_point!=null:
				tween.interpolate_property(camera_point, "position",
				camera_point.position, camera_point.position+Vector2(0,320*count), 1,
				Tween.TRANS_CIRC, Tween.EASE_IN_OUT)
		d.UP:
			pos -= 5*count
			tween.interpolate_property(self, "position",
			position, position+Vector2(0,-320*count), 1,
			Tween.TRANS_CIRC, Tween.EASE_IN_OUT)
			if camera_point!=null:
				tween.interpolate_property(camera_point, "position",
				camera_point.position, camera_point.position+Vector2(0,-320*count), 1,
				Tween.TRANS_CIRC, Tween.EASE_IN_OUT)
	tween.start()

func get_doors():
	var list = []
	for u in get_children():
		if u.is_in_group("door"):
			list.append(u)
	return list

func get_keys():
	var list = []
	for u in get_children():
		if u.is_in_group("key"):
			list.append(u)
	return list

func _on_Tween_tween_all_completed():
	find_parent("Level").set_shifting(false)
