extends KinematicBody2D

var speed = 100
var gravity = 500
var jump_force = -250

var movement :Vector2
var movy = 0
var jump = false

var is_shifting = false

var can_rotate = false

func _input(event):
	if event.is_action_pressed("rotate") && is_on_floor() && can_rotate:
		$Rotate.play()
		find_parent("Level").rotate_room()

func _physics_process(delta):
	if is_shifting:
		return
	
	var movx = Input.get_action_strength("right")-Input.get_action_strength("left")
	
	if movy < 0.2:
		$AnimatedSprite.animation = "jump"
	elif movy > 5:
		$AnimatedSprite.animation = "fall"
	elif movx == 0:
		$AnimatedSprite.animation = "idle"
	else:
		$AnimatedSprite.animation = "run"
		if movx < 0:
			$AnimatedSprite.flip_h = true
		if movx >0:
			$AnimatedSprite.flip_h = false
		
	
	if is_on_floor():
		movy = 5
		if Input.is_action_pressed("jump"):
			$Jump.play()
			movy = jump_force
	else:
		movy += gravity*delta
	
	movement = Vector2(movx*speed,movy)
	
	move_and_slide(movement,Vector2.UP)

func set_shifting(v):
	is_shifting = v
	$CollisionShape2D.set_deferred("disabled",v)
	print(v)

func add_item():
	$Item.play()
	can_rotate = true
