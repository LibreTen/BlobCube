extends Control


func _input(event):
	if event.is_action_pressed("enter") && $Sprite.frame == $Sprite.hframes-1:
		get_tree().change_scene("res://Level.tscn")
	elif event.is_action_pressed("ui_right") || event.is_action_pressed("right"):
		$Sprite.frame +=1
	elif event.is_action_pressed("ui_left") || event.is_action_pressed("left"):
		$Sprite.frame -= 1
		
	
	$Label.hide()
	$Label2.hide()
	$Label3.hide()
	$Label4.hide()
	$Label5.hide()
	
	match $Sprite.frame:
		0:
			$Label.show()
		1:
			$Label2.show()
		2:
			$Label3.show()
		3:
			$Label4.show()
		4:
			$Label5.show()

