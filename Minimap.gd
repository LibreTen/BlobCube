extends Node2D

var tex = preload("res://minimap.png")
onready var level = find_parent("Level")

func add_room(pos,color,n):
	var room = Sprite.new()
	room.name = n
	room.texture = tex
	room.modulate = color
	room.self_modulate = Color.black
	add_child(room)
	room.centered = false
	room.position.x = pos%5*16
	room.position.y = pos/5*16

func change_cursor(pos,room):
	$Cursor.position.x =pos%5*16
	$Cursor.position.y = pos/5*16
	get_node(str(room)).self_modulate = Color.white
	

func switch(room_name,new_pos):
	var room = get_node(str(room_name))
	room.position.x = new_pos%5*16
	room.position.y = new_pos/5*16
