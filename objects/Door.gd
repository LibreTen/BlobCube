extends StaticBody2D

export var key = 0

func _ready():
	$Label.text = str(key)

func _on_Area2D_body_entered(body):
	if body.is_in_group("player") && find_parent("Level").has_key(key):
		$CollisionShape2D.set_deferred("disabled",true)
		$Sprite.frame = 1

func _on_Area2D_body_exited(body):
	if body.is_in_group("player"):
		$Sprite.frame = 0

func set_value(v):
	key = v
	$Label.text = str(key)
