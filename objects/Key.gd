extends Area2D

export var value = 0

func _ready():
	$Label.text = str(value)

func _on_Key_body_entered(body):
	if body.is_in_group("player"):
		find_parent("Level").add_key(value)
		queue_free()

func set_value(v):
	value = v
	$Label.text = str(value)
