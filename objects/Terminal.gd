extends Area2D

export(String, MULTILINE) var text = ""
export var counts_as_log = true
var activated = false

# Called when the node enters the scene tree for the first time.
func _ready():
	$Text.hide()
	$Text.text = text


func _on_Area2D_body_exited(body):
	if body.is_in_group("player"):
		$Text.hide()


func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		$Terminal.frame = 0
		if !activated:
			activated = true
			find_parent("Level").add_log()
		$Text.show()
