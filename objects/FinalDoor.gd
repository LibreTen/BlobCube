extends Area2D


func _input(event):
	if event.is_action_pressed("interact") && find_parent("Level").has_all_logs():
		for u in get_overlapping_bodies():
			if u.is_in_group("player"):
				get_tree().change_scene("res://End.tscn")
				return
