extends Area2D

enum t {
	SWITCH
}

enum d {UP,DOWN,LEFT,RIGHT}

export(t) var type
export(d) var dir
export var count = 1

func _ready():
	match dir:
		d.UP:
			$Sprite.frame = 2
		d.DOWN:
			$Sprite.frame = 3
		d.RIGHT:
			$Sprite.frame = 0
		d.LEFT:
			$Sprite.frame = 1
	

func _input(event):
	if event.is_action_pressed("interact") && !find_parent("Level").is_shifting:
		for u in get_overlapping_bodies():
			if u.is_in_group("player"):
				if !u.is_on_floor():
					return
				find_parent("Level").shift(int(get_parent().name),type,dir)
				break
		



func _on_Shifter_body_entered(body):
	if body.is_in_group("player"):
		find_parent("Level").set_checkpoint(self)
