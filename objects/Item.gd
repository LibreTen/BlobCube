extends Area2D


func _on_Item_body_entered(body):
	if body.is_in_group("player"):
		body.add_item()
		queue_free()
