extends Node2D

var size_x = 5
var size_y = 5
var rooms = []
var current_room :int
var current_room_position :int

var is_shifting = false

onready var player : KinematicBody2D = $Player
onready var camera_point : Position2D = $Position2D

var checkpoint :Node2D
var saved_room
var old_room : Node2D
var old_room_position
var old_rotation

var logs_collected = 0
var max_logs = 10

var keys = []

enum t {SWITCH}
enum d {UP,DOWN,LEFT,RIGHT}

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize_rooms()
	change_room(0)
#	remove_child(player)
#	get_node("Rooms/0").add_child(player)
	player.position = Vector2(0,-30)

func _process(delta):
#	if player.position.x > 320:
#		change_room(find_next_room(1))
#	elif player.position.x < 0:
#		change_room(find_next_room(-1))
#
#	if player.position.y > 320:
#		change_room(find_next_room(size_x))
#	elif player.position.y < 0:
#		change_room(find_next_room(-size_x))
	
	if player.global_position.x > (current_room_position%size_x+1)*320:
		change_room(find_next_room(1))
	elif player.global_position.x < current_room_position%size_x*320:
		change_room(find_next_room(-1))

	if player.global_position.y > (current_room_position/size_y+1)*320:
		change_room(find_next_room(size_x))
	elif player.global_position.y < current_room_position/size_y*320:
		change_room(find_next_room(-size_x))

func randomize_rooms():
	var room_count = size_x*size_y
	for i in room_count:
		rooms.append(i)
	randomize()
	rooms.shuffle()
	var door_list = []
	var key_list = []
	var i :int = 0
	for u in rooms:
		var room_base : PackedScene = load("res://rooms/"+str(u)+".tscn")
		var room :Node2D = room_base.instance()
		var root_node = Node2D.new()
		root_node.name = str(u)
		room.name = str(u)
#		$Rooms.add_child(room)
		$Rooms.add_child(root_node)
		root_node.add_child(room)
		root_node.set_script(load("res://rooms/room.gd"))
		root_node.pos = i
		root_node.position.x = i%size_x*320+160
		root_node.position.y = i/size_y*320+160
		room.position.x = -160
		room.position.y = -160
#		room.pos = i
#		room.position.x = i%size_x*320
#		room.position.y = i/size_y*320
		$CanvasLayer/ViewportContainer/Viewport/Minimap.add_room(i,room.get_node("TileMap").modulate,str(u))
		door_list+= room.get_doors()
		key_list += room.get_keys()
		i+=1
	
	door_list.shuffle()
	key_list.shuffle()
	
	i = 0
	for u in key_list:
		u.set_value(i)
		i+=1
	
	i=0
	for u in door_list:
		u.set_value(i)
		i+=1

func change_room(new_room):
	if new_room == null:
		return
	if is_shifting:
		return
	
	var old_pos = player.global_position
	player.get_parent().remove_child(player)
	get_node("Rooms/"+str(new_room)).add_child(player)
	player.global_position = old_pos
	
	current_room = new_room
	current_room_position = find_room_position(new_room)
	camera_point.position.x = current_room_position%size_x*320+160
	camera_point.position.y = current_room_position/size_y*320+160
	$CanvasLayer/ViewportContainer/Viewport/Minimap.change_cursor(current_room_position,current_room)
	player.global_rotation = 0

func find_next_room(offset,custom_room = null):
	var new_pos
	if custom_room == null:
		new_pos = current_room_position+offset
	else:
		var pos = get_node("Rooms/"+str(custom_room)).pos
		new_pos = pos+offset
	for u in $Rooms.get_children():
		if u.pos == new_pos:
			var re = int(u.name)
			if re != null:
				return re
			elif custom_room != null:
				return custom_room
			else:
				return current_room

func find_room_position(id):
	var room = get_node("Rooms/"+str(id))
	return room.pos

func set_shifting(v):
	is_shifting = v
	player.set_shifting(v)

func shift(room,type,dir):
	var next_room
	var pos = find_room_position(room)
	if type == t.SWITCH:
		match dir:
			d.RIGHT:
				if pos %5 == 4:
					return
				next_room = find_next_room(1,room)
			d.LEFT:
				if pos %5 == 0:
					return
				next_room = find_next_room(-1,room)
			d.UP:
				if pos <5:
					return
				next_room = find_next_room(-5,room)
			d.DOWN:
				if pos>19:
					return
				next_room = find_next_room(5,room)
	
	
	var room1 = get_node("Rooms/"+str(room))
	var room2 = get_node("Rooms/"+str(next_room))
	if room2 == null:
		return
	$Sfx/Shift.play()
	room1.move(dir,1,camera_point)
	room2.move(dir,-1,null)
	set_shifting(true)
	$CanvasLayer/ViewportContainer/Viewport/Minimap.switch(room,room1.pos)
	$CanvasLayer/ViewportContainer/Viewport/Minimap.switch(next_room,room2.pos)
	


func set_checkpoint(node):
	checkpoint = node
	saved_room = current_room

func load_checkpoint():
	player.global_position = checkpoint.global_position
	$Sfx/Die.play()
	#change_room(saved_room)

func _input(event):
	if event.is_action_pressed("reset"):
		load_checkpoint()

func add_key(value:int):
	keys.append(value)
	var key :TextureRect= load("res://objects/KeyDisplay.tscn").instance()
	$CanvasLayer/VBoxContainer.add_child(key)
	key.get_child(0).text = str(value)
	$Sfx/Item.play()

func has_key(value:int):
	return keys.has(value)

func rotate_room():
	if is_shifting:
		return
	var room :Node2D = get_node("Rooms/"+str(current_room))
	set_shifting(true)
	$Tween.interpolate_property(room, "rotation_degrees",
			room.rotation_degrees, room.rotation_degrees+90, 1,
			Tween.TRANS_CIRC, Tween.EASE_IN_OUT)
	$Tween.start()

#func rotate_room():
#	var room :Node2D = get_node("Rooms/"+str(current_room))
#	old_room_position = room.position
#	$Rooms.remove_child(room)
#	var rot_center = Node2D.new()
#	rot_center.global_position = camera_point.global_position
#	add_child(rot_center)
#	rot_center.add_child(room)
#	room.global_position = old_room_position
#	old_rotation = room.global_rotation
#	$Tween.interpolate_property(rot_center, "rotation_degrees",
#			rotation_degrees, rotation_degrees+90, 1,
#			Tween.TRANS_CIRC, Tween.EASE_IN_OUT)
#	$Tween.start()
#	old_room = room
#
#
func _on_Tween_tween_all_completed():
	set_shifting(false)
	player.global_rotation = 0
#	old_room.get_parent().remove_child(old_room)
#	$Rooms.add_child(old_room)
#	old_room.global_position = old_room_position
#	old_room.global_rotation = old_rotation+PI/2

func add_log():
	logs_collected +=1
	$CanvasLayer/Sprite/Label.text = str(logs_collected)+"/"+str(max_logs)
	$Sfx/Log.play()
	if logs_collected == max_logs:
		$Sfx/OpenFinalDoor.play()
		get_node("Rooms/24/24/FinalDoor/Sprite").frame = 1

func has_all_logs():
	return logs_collected >= max_logs

func _on_AudioStreamPlayer_finished():
	$AudioStreamPlayer.stream = load("res://sfx/bgm.ogg")
	$AudioStreamPlayer.play()
